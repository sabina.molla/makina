from django.shortcuts import render
from vehicles.models import Vehicle



# Create your views here.

def get_vehicles(request):
    vehicles = Vehicle.objects.all()
    return render(request, "vehicle_list.html",context={
        
    })


def get_vehicle_details (request,pk):
    #select * from vehicles_vehicle where id=%s
    vehicle = Vehicle.objects.get (pk=pk)
    return render ( request, "vehicle_detail.html", context={
        "vehicle": vehicles
    })