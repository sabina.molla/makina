from django.urls import path

from vehicles.views import get_vehicle_details, get_vehicles

urlpatterns=[
    path ("vehicles",get_vehicles,name="get_vehicles"),
    path('vehicles/<int:pk>',get_vehicle_details,name="vehicle_detail"),
]