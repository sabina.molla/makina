from django.db import models


# CREATE TABLE vehicles ()
class Vehicle(models.Model):
    AUTOMATIC="Automatic"
    MANUAL="Manual"
    TRANSMISSION_CHOICES=[
        (AUTOMATIC,"Automatic"),
        (MANUAL,"Manual"),
    ]
    #brand varchar (100)
    brand=models.CharField (max_length=100,null=False)
    #brand varchar(100) not null
    model =models.CharField(max_length=100,null=False)
    #year int not null constraint chk_positive check (year>0)
    year=models.PositiveBigIntegerField(null=False)

    transmission=models.CharField(max_length=32,null=True,choices=TRANSMISSION_CHOICES)


    def __str__(self):
        return f'{self.brand} {self.model}'



